<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>The Chemistry Lab</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="page-wrapper">
    <div id="header-wrapper">
        <div class="container">
            <div class="row">
                <div class="12u">

                    <header id="header">
                        <h1><a href="#" id="logo">The Chemistry Lab</a></h1>
                        <nav id="nav">
                            <a href="{{ url('/') }}">Inicio</a>
                            <a href="{{ url('semana1') }}">Cursos</a>
                            @if (Auth::guest())
                                <a href="{{ url('login') }}">Iniciar sesion</a>
                                <a href="{{ url('register') }}">Registrarse</a>
                                @else
                                <a href="{{ url('home') }}">Perfil</a>
                            @endif
                        </nav>
                    </header>

                </div>
            </div>
        </div>
    </div>

    @yield('content')

    <div id="footer-wrapper">
        <div class="container">
            <div class="row">
                <div class="12u">

                    <div id="copyright">
                        &copy; Derechos reservados. | The Chemistry Lab</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/skel-viewport.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>