@extends('layouts.app')

@section('content')
    <style>
        input[type="submit"]
        {
            background: transparent;
            border: none;
        }
    </style>
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="8u 12u(mobile)">

                    <section class="left-content">
                        <h2>Bienvenido a tu perfil, {{ Auth::user()->name }}</h2>
                    </section>

                </div>
                <div class="4u 12u(mobile)">
                    <section>
                        <div>
                            <div class="row">
                                <div class="12u 12u(mobile)">
                                    <ul class="link-list">
                                        <li>
                                            <form method="post" action="{{ url('/logout') }}">
                                                {{ csrf_field() }}
                                                <input type="submit" value="Cerrar sesion">
                                            </form>
                                        </li>
                                        <li>
                                            <form method="get" action="{{ url('semana1') }}">
                                                <input type="submit" value="Tus cursos">
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
