@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="4u 12u(mobile)">
                    <section>
                        <ul class="small-image-list">
                            {{--<li>--}}
                                {{--<a href="{{ url('/semana2') }}"><img src="images/semanas/flechaderecha.png" alt="" class="left" /></a>--}}
                                {{--<h4>Siguiente curso</h4>--}}
                                {{--<p>Una vez acabado este curso, continua con el proximo.</p>--}}
                            {{--</li>--}}
                            <li>
                                <a href="#"><img src="images/pic1.jpg" alt="" class="left" /></a>
                                <h4>Mañana.</h4>
                                <p>Espera a mañana para seguir con el siguiente curso.</p>
                            </li>
                            <li>
                                <a href="{{ url('/semana1Actividad') }}"><img src="images/semanas/actividad.png" alt="" class="left" /></a>
                                <h4>Actividad</h4>
                                <p>Para fortalecer lo que aprendimos haz esta actividad.</p>
                            </li>
                        </ul>
                    </section>
                </div>
                <div class="8u 12u(mobile) important(mobile)">

                    <section class="right-content">
                        <h2>ELEMENTOS, COMPUESTOS Y MEZCLAS</h2>
                        <h3>INTRODUCCIÓN AL TEMA</h3>
                        <p>
                            ELEMENTOS:
                            Las sustancias son los materiales con los que trabaja el químico y éstas pueden ser puras o no. Las sustancias puras se clasifican en elementos y compuestos.
                        </p>
                        <p>
                            Ejemplos:<br>
                            <img src="images/semanas/foto1.jpg" alt="" style="height: 20%; width: 20%"/>
                            (Elemento)
                        </p>
                        <p>
                            Los elementos son sustancias simples que no pueden descomponerse por métodos químicos ordinarios.
                            (Ejemplo de método simple para descomponer sustancia.)
                        </p>
                        <p>
                            <img src="images/semanas/foto2.gif" alt="" style="height: 50%; width: 50%"/>
                        </p>
                        <p>
                            La mínima unidad material que representa las características de un elemento es el átomo. Un elemento posee átomos iguales entre sí y diferentes a los de otro elemento.
                            Ejemplo:
                        </p>
                        <p>
                            <img src="images/semanas/foto3.png" alt="" style="height: 50%; width: 50%"/><br>
                        </p>
                        <p>
                            (Diferencias entre átomos de diferente elemento)
                            Desde la antigüedad se conocen varios elementos, algunos son muy abundantes, otros son muy raros, algunos son radiactivos y otros se han sintetizado en el laboratorio y tienen una vida muy corta.
                            Ejemplo:
                        </p>
                        <p>
                            <img src="images/semanas/foto4.jpg" alt="" style="height: 50%; width: 50%"/><br>
                        </p>
                        <p>
                            <h4>Video explicativo:</h4>
                            <iframe width="420" height="315"
                                    src="https://www.youtube.com/embed/dMKa86uTFJQ">
                            </iframe>
                        </p>
                        <p>
                            COMPUESTOS:
                            Los compuestos son sustancias que resultan de la unión química de dos o más elementos en proporciones definidas, se combinan de tal manera que ya no es posible identificarlos por sus propiedades originales e individuales y solamente por medio de una acción química se les puede separar.
                            Ejemplo:
                        </p>
                        <p>
                            <img src="images/semanas/foto5.jpg" alt="" style="height: 20%; width: 20%"/><br>
                        </p>

                        <p>
                            (Compuesto.)
                            Los compuestos se representan con fórmulas y la mínima unidad material que simboliza las características del compuesto es la molécula. Por ejemplo: ácido sulfúrico (H2SO4), cloruro de sodio (NaCl), amoníaco (NH3) y agua (H2O)

                        </p>
                        <p>
                            <h4>Video explicativo:</h4>
                            <iframe width="420" height="315"
                                    src="https://www.youtube.com/embed/VmAm44Gkjjw">
                            </iframe>
                        </p>
                        <p>
                            MEZCLAS:

                            Las mezclas son el resultado de la unión física de dos o más sustancias a las cuales se les llama componentes, éstos pueden ser elementos o compuestos, y al efectuarse dichas mezclas conservan sus propiedades individuales.

                            Ejemplo:
                        </p>
                        <p>
                            <img src="images/semanas/foto6.png" alt="" style="height: 50%; width: 50%"/><br>
                        </p>
                        <p>
                            (Mezclas)

                            La composición de las mezclas es variable y sus componentes podrán separarse por medios físicos o mecánicos debido a que no están unidos químicamente.
                        </p>
                        <p>
                            <img src="images/semanas/foto7.jpg" alt="" style="height: 50%; width: 50%"/><br>
                        </p>
                        <p>
                            Las mezclas se pueden clasificar en homogéneas y heterogéneas.

                            Mezclas homogéneas: Son aquéllas cuyos componentes forman una sola fase y no se puede distinguir un componente de otro.
                        </p>
                        <p>
                            <img src="images/semanas/foto8.jpg" alt="" style="height: 50%; width: 50%"/><br>
                        </p>
                        <p>
                            Ejemplo: la sangre, el agua de mar o una solución alcohólica.

                            Mezclas heterogéneas: Son aquéllas cuyos componentes no forman una fase homogénea, es decir, no hay una distribución uniforme de los mismos.
                        </p>
                        <p>
                            <img src="images/semanas/foto9.jpg" alt="" style="height: 50%; width: 50%"/><br>
                        </p>
                        <h3>Ejemplo: arena, limadura de hierro, agua.</h3>
                        <p>
                            <h4>Video explicativo:</h4>
                            <iframe width="420" height="315"
                                    src="https://www.youtube.com/embed/KpJjQBuRLDU">
                            </iframe>
                        </p>
                        <h2>
                            Actividad 1: elaborar un cuadro comparativo con imágenes sobre las diferencias entre elementos, mezclas y compuestos.
                        </h2>
                    </section>

                </div>
            </div>
        </div>
    </div>
@endsection
