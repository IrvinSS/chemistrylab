@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="4u 12u(mobile)">
                    <section>
                        <ul class="small-image-list">
                            {{--<li>--}}
                            {{--<a href="{{ url('/semana2') }}"><img src="images/semanas/flechaderecha.png" alt="" class="left" /></a>--}}
                            {{--<h4>Siguiente curso</h4>--}}
                            {{--<p>Una vez acabado este curso, continua con el proximo.</p>--}}
                            {{--</li>--}}
                            <li>
                                <a href="#"><img src="images/pic1.jpg" alt="" class="left" /></a>
                                <h4>Mañana.</h4>
                                <p>Espera a mañana para seguir con el siguiente curso.</p>
                            </li>
                            <li>
                                <a href="#"><img src="images/semanas/actividad.png" alt="" class="left" /></a>
                                <h4>Actividad</h4>
                                <p>Para fortalecer lo que aprendimos haz esta actividad.</p>
                            </li>
                        </ul>
                    </section>
                </div>
                <div class="8u 12u(mobile) important(mobile)">

                    <section class="right-content">
                        <h2>CÁLCULO DE LA MASA Y EL VOLUMEN A PARTIR DE ECUACIONES QUÍMICAS</h2>
                        <p>Las ecuaciones químicas permiten calcular, a partir de una cantidad determinada
                            de alguno de los reactivos y productos que intervienen en una reacción, la
                            cantidad necesaria del resto de los componentes de la misma.</p>
                        <p>- Cálculos masa - masa</p>
                        <p> En este caso nos aprovechamos de la relación que hay entre  cantidad de sustancia
                            (en mol), masa de sustancia y masa molar, tal como indica la relación:
                        </p>
                            <img src="images/semanas/foto19.png" alt="" style="height: 100%; width: 100%"/><br>
                        <p>Ejemplos de aplicación de las formulas:<br>
                            m=150gr<br>
                            n=?<br>
                            AL2(SO4)3<br>

                            Sustitución:<br>
                            Al: 27x2=54<br>
                            S: 32x3=96<br>
                            O: 16x12=192<br>
                            Suma: 54+96+192=342<br>
                            n=150gr/342=0.434<br>
                            n=0.434<br><br>
                            M=?         <br>
                            n=35 mol<br>
                            NaOh<br>
                            V=2 litro.<br>
                            M=35/2=1.75
                        </p>
                        <h2>Actividad 4: calcular</h2>
                        <h4>
                            1.-La Masa Molar del metanol: CH3OH. 
                        </h4>
                        <h4>
                            2.-La masa molecular de Fe2 (SO4)3
                        </h4>
                        <p>Recuerda que las actividades se envian al correo electronico</p>
                        <p>
                            <form method="POST" action="{{ url('subir') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <label for="tarea">Subir tarea<input type="file" name="tarea"></label>
                                <input type="submit" class="btn btn-primary" value="Subir Tarea">
                            </form>
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
