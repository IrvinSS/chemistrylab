@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="4u 12u(mobile)">
                    <section>
                        <ul class="small-image-list">
                            <li>
                                <a href="{{ url('/semana4') }}"><img src="images/semanas/flechaderecha.png" alt="" class="left" /></a>
                                <h4>Siguiente curso</h4>
                                <p>Una vez acabado este curso, continua con el proximo.</p>
                            </li>
                            <li>
                                <a href="#"><img src="images/semanas/actividad.png" alt="" class="left" /></a>
                                <h4>Actividad</h4>
                                <p>Para fortalecer lo que aprendimos haz esta actividad.</p>
                            </li>
                            <li>
                                <a href="{{ url('/semana2') }}"><img src="images/semanas/flechaizquierda.png" alt="" class="left" /></a>
                                <h4>Siguiente curso</h4>
                                <p>Una vez acabado este curso, continua con el proximo.</p>
                            </li>
                        </ul>
                    </section>
                </div>
                <div class="8u 12u(mobile) important(mobile)">

                    <section class="right-content">
                        <h2>LAS REACCIONES QUÍMICAS</h2>
                        <h3>Los cambios en la materia</h3>
                        <p>
                            La materia puede sufrir cambios mediante diversos procesos. No obstante, todos esos cambios se pueden agrupar en dos tipos: cambios físicos y cambios químicos.
                        </p>
                        <h3>1.1- CAMBIOS FÍSICOS</h3>
                        <p>
                            En estos cambios no se producen modificaciones en la naturaleza de la sustancia o sustancias que intervienen. Ejemplos de este tipo de cambios son:
                            Cambios de estado.
                            Mezclas.
                            Disoluciones.
                            Separación de sustancias en mezclas o disoluciones.
                        </p>
                        <h3>1.2- CAMBIOS QUÍMICOS</h3>
                        <p>
                            En este caso, los cambios si alteran la naturaleza de las sustancias: desaparecen unas y aparecen otras con propiedades muy distintas. No es posible volver atrás por un procedimiento físico (como calentamiento o enfriamiento, filtrado, evaporación, etc.)
                            Una reacción química es un proceso por el cual una o más sustancias, llamadas reactivos, se transforman en otra u otras sustancias con propiedades diferentes, llamadas productos.
                            En una reacción química, los enlaces entre los átomos que forman los reactivos se rompen. Entonces, los átomos se reorganizan de otro modo, formando nuevos enlaces y dando lugar a una o más sustancias diferentes a las iniciales.
                            LOS CAMBIOS EN LA MATERIA

                            La materia puede sufrir cambios mediante diversos procesos. No obstante, todos esos cambios se pueden agrupar en dos tipos: cambios físicos y cambios químicos.
                        </p>
                        <h3>2.- CARACTERÍSTICAS DE LAS REACCIONES QUÍMICAS</h3>
                        <p>
                            A) La o las sustancias nuevas que se forman suelen presentar un aspecto totalmente diferente del que tenían las sustancias de partida.<br>
                            B) Durante la reacción se desprende o se absorbe energía:<br>
                            Reacción exotérmica: se desprende energía en el curso de la reacción.<br>
                            Reacción endotérmica: se absorbe energía durante el curso de la reacción.<br>
                            C) Se cumple la ley de conservación de la masa: la suma de las masas de los reactivos es igual a la suma de las masas de los productos. Esto es así porque durante la reacción los átomos ni aparecen ni desaparecen, sólo se reordenan en una disposición distinta.
                        </p>
                        <h3>3.- ECUACIONES QUÍMICAS</h3>
                        <p>
                            Una reacción química se representa mediante una ecuación química. Para leer o escribir una ecuación química, se deben seguir las siguientes reglas:

                            Las fórmulas de los reactivos se escriben a la izquierda, y las de los productos a la derecha, separadas ambas por una flecha que indica el sentido de la reacción.

                            A cada lado de la reacción, es decir, a derecha y a izquierda de la flecha, debe existir el mismo número de átomos de cada elemento.
                            Cuando una ecuación química cumple esta segunda regla, se dice que está ajustada o equilibrada. Para equilibrar reacciones químicas, se ponen delante de las fórmulas unos números llamados coeficientes, que indican el número relativo de átomos y moléculas que intervienen en la reacción.
                            Nota: estos coeficientes situados delante de las fórmulas, son los únicos números en la ecuación que se pueden cambiar, mientras que los números que aparecen dentro de las fórmulas son intocables, pues un cambio en ellos significa un cambio de sustancia que reacciona y, por tanto, se trataría de una reacción distinta.

                            Si se quiere o necesita indicar el estado en que se encuentran las sustancias que intervienen o si se encuentran en disolución, se puede hacer añadiendo los siguientes símbolos detrás de la fórmula química correspondiente.
                        </p>
                        <h3>Actividad 2.</h3>
                        <p>
                            Realizar una tabla con  las notaciones que  más se utilizan en las ecuaciones. Pueden ser representando los estados de la materia, las direcciones de la reacción, etc.
                            <br>Ejemplo:<br>
                            (s) = sólido.<br>
                            (Metal) = elemento metálico.
                        </p>
                    </section>

                </div>
            </div>
        </div>
    </div>
@endsection
