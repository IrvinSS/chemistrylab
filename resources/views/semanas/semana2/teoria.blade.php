@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="4u 12u(mobile)">
                    <section>
                        <ul class="small-image-list">
                            <li>
                                <a href="{{ url('/semana3') }}"><img src="images/semanas/flechaderecha.png" alt="" class="left" /></a>
                                <h4>Siguiente curso</h4>
                                <p>Una vez acabado este curso, continua con el proximo.</p>
                            </li>
                            <li>
                                <a href="#"><img src="images/semanas/actividad.png" alt="" class="left" /></a>
                                <h4>Actividad</h4>
                                <p>Para fortalecer lo que aprendimos haz esta actividad.</p>
                            </li>
                            <li>
                                <a href="{{ url('/semana1') }}"><img src="images/semanas/flechaizquierda.png" alt="" class="left" /></a>
                                <h4>Siguiente curso</h4>
                                <p>Una vez acabado este curso, continua con el proximo.</p>
                            </li>
                        </ul>
                    </section>
                </div>
                <div class="8u 12u(mobile) important(mobile)">

                    <section class="right-content">
                        <h2>EXPLICACIÓN COMPLEMENTRARIA:</h2>
                        <p>
                            Continuando con el tema elementos, compuestos y mesclas.
                        </p>
                        <p>
                            <img src="images/semanas/foto10.jpg" alt="" style="height: 50%; width: 50%"/><br>
                        </p>
                        <p>
                            <h4>Veremos este video:</h4>
                            <iframe width="420" height="315"
                                    src="https://www.youtube.com/embed/TvhQDmBvQgE">
                            </iframe>
                        </p>
                    </section>

                </div>
            </div>
        </div>
    </div>
@endsection
