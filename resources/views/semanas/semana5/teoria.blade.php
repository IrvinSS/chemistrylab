@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="4u 12u(mobile)">
                    <section>
                        <ul class="small-image-list">
                            <li>
                                <a href="#"><img src="images/semanas/actividad.png" alt="" class="left" /></a>
                                <h4>Actividad</h4>
                                <p>Para fortalecer lo que aprendimos haz esta actividad.</p>
                            </li>
                            <li>
                                <a href="{{ url('/semana4') }}"><img src="images/semanas/flechaizquierda.png" alt="" class="left" /></a>
                                <h4>Siguiente curso</h4>
                                <p>Una vez acabado este curso, continua con el proximo.</p>
                            </li>
                        </ul>
                    </section>
                </div>
                <div class="8u 12u(mobile) important(mobile)">

                    <section class="right-content">
                        <h2>COMPOSICIONES PORCENTUALES Y FÓRMULAS QUÍMICAS</h2>
                        <p>
                            Una ley fundamental de la química afirma que en todo compuesto químico que esté formado por dos o más elementos diferentes, éstos se encuentran presentes en dicho compuesto en una cantidad o composición porcentual determinada. Lo que quiere decir, por ejemplo, que el hidróxido de aluminio Al(OH)3 que se obtenga en España tendrá el mismo porcentaje de aluminio, de oxígeno y de hidrógeno que el que se pueda obtener en cualquier otra parte del mundo.
                            LA COMPOSICIÓN PORCENTUAL A TRAVÉS DE LA FÓRMULA QUÍMICA
                            Conocida la fórmula de un compuesto químico, es posible saber el porcentaje de masa con el que cada elemento que forma dicho compuesto está presente en el mismo.
                            Ejemplo:
                            Una molécula de dióxido de azufre, SO2, contiene un átomo de azufre y dos de oxígeno. Calcular la composición en tanto por ciento de dicha molécula.
                            Datos: la masa atómica del azufre es 32,1 y la del oxígeno, 16,0 u.
                            El problema puede resolverse por dos vías:
                            Utilizando unidades de masa atómica:
                            Masa molecular del SO2 = (32,1) + (2 · 16) = 64,1 u.
                            Porcentaje de azufre en el compuesto:
                        </p>
                        <p>
                            <img src="images/semanas/foto12.png" alt="" style="height: 50%; width: 50%"/>
                        </p>
                        <p>
                            Porcentaje de oxígeno en el compuesto:
                        </p>
                        <p>
                            <img src="images/semanas/foto13.png" alt="" style="height: 50%; width: 50%"/>
                        </p>
                        <p>
                            Utilizando gramos:
                            1 mol de moléculas de SO2 (64,1 g) contiene 1 mol de átomos de azufre (32,1 g) y 2 moles de átomos de oxígeno (16,0 g).
                            Porcentaje de azufre en el compuesto:
                            Si en 64,1 g de SO2 hay  32,1 g de azufre, en 100 g habrá   x, luego


                            Porcentaje de oxígeno en el compuesto:
                            Si en 64,1 g de SO2 hay  32,0 g de oxígeno, en 100 g habrá  x, luego

                            La fórmula química de un compuesto a través de su composición porcentual
                            Conocida la composición porcentual de un compuesto o su composición elemental en gramos, se puede determinar su fórmula más simple mediante cálculos elementales.
                            La fórmula más simple o fórmula empírica de un compuesto es la menor relación entre el número de átomos presentes en una molécula de ese compuesto.
                            A través de la composición porcentual de un compuesto, puede conocerse su fórmula empírica.






                            Ejemplo:

                            El análisis de una muestra de un compuesto puro revela que contiene un 27,3% de carbono y un 72,7% de oxígeno en masa. Determinar la fórmula empírica de ese compuesto.

                            Para resolver el problema consideramos 100 g del compuesto. Dada la composición porcentual del mismo, de esos 100 g corresponden 27,3 al carbono y 72,7 al oxígeno. Con ello, se puede calcular el número de moles de átomos de cada elemento:
                        </p>
                        <p>
                            <img src="images/semanas/foto14.png" alt="" style="height: 50%; width: 50%"/>
                        </p>
                        <p>
                            <img src="images/semanas/foto15.png" alt="" style="height: 50%; width: 50%"/>
                        </p>
                            Dividiendo los dos números obtenidos se llega a una relación empírica entera entre ambos, a partir de la cual se tiene la relación de átomos en la fórmula empírica:

                        <p>
                            <img src="images/semanas/foto16.png" alt="" style="height: 50%; width: 50%"/>
                        </p>
                        <p>
                            <img src="images/semanas/foto17.png" alt="" style="height: 20%; width: 20%"/>
                        </p>
                            La fórmula empírica corresponde al CO2, dióxido de carbono.


                            EL VOLUMEN MOLAR
                            En los cálculos con gases es conveniente adoptar una unidad universal de volumen: el volumen molar. Se denomina así al volumen de cualquier gas, medido en condiciones normales de presión y temperatura (1 atmósfera y0 ºC). Este volumen molar tiene un valor de 22,4 l.
                        <p>
                            <img src="images/semanas/foto18.png" alt="" style="height: 80%; width: 80%"/>
                        </p>
                        <h2>Actividad 3: investigar el número de Avogadro y su  relación del número de partículas presentes en los compuestos.</h2>
                    </section>

                </div>
            </div>
        </div>
    </div>
@endsection
