@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="4u 12u(mobile)">
                    <section>
                        <ul class="small-image-list">
                            <li>
                                <a href="{{ url('/semana5') }}"><img src="images/semanas/flechaderecha.png" alt="" class="left" /></a>
                                <h4>Siguiente curso</h4>
                                <p>Una vez acabado este curso, continua con el proximo.</p>
                            </li>
                            <li>
                                <a href="#"><img src="images/semanas/actividad.png" alt="" class="left" /></a>
                                <h4>Actividad</h4>
                                <p>Para fortalecer lo que aprendimos haz esta actividad.</p>
                            </li>
                            <li>
                                <a href="{{ url('/semana3') }}"><img src="images/semanas/flechaizquierda.png" alt="" class="left" /></a>
                                <h4>Siguiente curso</h4>
                                <p>Una vez acabado este curso, continua con el proximo.</p>
                            </li>
                        </ul>
                    </section>
                </div>
                <div class="8u 12u(mobile) important(mobile)">

                    <section class="right-content">
                        <h2>VELOCIDAD DE UNA REACCIÓN QUÍMICA</h2>
                        <p>
                            Para saber si una reacción es rápida o lenta, hay que conocer la velocidad a la que transcurre.  Podemos definir velocidad de reacción como la variación de cantidad de sustancia formada o transformada por unidad de tiempo.
                            En general, para determinar la velocidad de una reacción, hay que medir la cantidad de reactivo que desaparece o la cantidad de producto que se forma por unidad de tiempo.
                        </p>

                        <h3>Factores que afectan a la velocidad de reacción</h3>
                        <p>
                            La velocidad de una reacción se ve influida por una serie de factores; entre ellos se pueden destacar:
                        </p>
                        <h3>Naturaleza de los reactivos</h3>
                        <p>
                            Se ha observado que según los reactivos que intervengan, las reacciones tienen distinta velocidad, pero no se ha podido establecer aún unas reglas generales.
                        </p>
                        <h3>Concentración de los reactivos</h3>
                        <p>
                            La velocidad de reacción aumenta con la concentración de los reactivos.  Para aumentar la concentración de un reactivo:
                            <h4>Si es un gas, se consigue elevando su presión.</h4>
                            <h4>Si se encuentra en disolución, se consigue cambiando la relación entre el soluto y el disolvente.</h4>
                        </p>
                        <h3>Superficie de contacto de los reactivos</h3>
                        <p>
                            Cuanto más divididos están los reactivos, más rápida es la reacción. Esto es así porque se aumenta la superficie expuesta a la misma.
                        </p>
                        <h3>Temperatura</h3>
                        <p>
                            En general, la velocidad de una reacción química aumenta conforme se eleva la temperatura.
                        </p>
                        <h3>Presencia de catalizadores</h3>
                        <p>
                            Un catalizador es una sustancia, distinta a los reactivos o los productos, que modifican la velocidad de una reacción. Al final de la misma, el catalizador se recupera por completo e inalterado. En general, hace falta muy poca cantidad de catalizador.
                            Los catalizadores aumentan la velocidad de la reacción, pero no la cantidad de producto que se forma.
                        </p>
                        <p>
                        <h4>Video explicativo:</h4>
                        <iframe width="420" height="315"
                                src="https://www.youtube.com/embed/mGoOBGufB-M">
                        </iframe>
                        </p>
                    </section>

                </div>
            </div>
        </div>
    </div>
@endsection
