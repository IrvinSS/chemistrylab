<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/semana1', 'HomeController@semana1');
Route::get('/semana1Actividad', 'HomeController@semana1Actividad');


//Route::get('/semana2', 'HomeController@semana2');
//
//Route::get('/semana3', 'HomeController@semana3');
//
//Route::get('/semana4', 'HomeController@semana4');
//
//Route::get('/semana5', 'HomeController@semana5');

Route::post('subir', 'HomeController@subir');