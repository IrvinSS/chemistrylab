<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function semana1()
    {
        return view('semanas/semana1/teoria');
    }

    public function semana1Actividad(){
        return view('semanas/semana1/actividad');
    }

    public function semana2()
    {
        return view('semanas/semana2/teoria');
    }

    public function semana3()
    {
        return view('semanas/semana3/teoria');
    }

    public function semana4()
    {
        return view('semanas/semana4/teoria');
    }

    public function semana5()
    {
        return view('semanas/semana5/teoria');
    }

    public function subir(Request $request)
    {
//        dd($request->file('tarea')->getFilename());
        if($request->file('tarea')->isValid()) {
            $request->file('tarea')->move('public', $request->file('tarea')->getFilename() . ".doc");
        }

        return view('home');
    }
}
